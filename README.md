# CoucDB backup restore scripts
Couchdb backup docs with attachments, one doc one file.

## Backup
    atombackup http://localhost:5984/database
A folder will be created with the database name

## Restore
    atomrestore http://localhost:5984/database
Where in current folder, must have a folder with database name

[www.fernandobhz.com.br](http://www.fernandobhz.com.br)
