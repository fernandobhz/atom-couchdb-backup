#! /usr/bin/env node

if (process.argv.length != 3) {
	console.log('Usage: atombackup url');
	return;
}

var url = process.argv[2];

process.on('unhandledRejection', function(err, p){ console.log('unhandledRejection'); console.log(err); });
process.on('uncaughtException', function(err) { console.log('uncaughtException'); console.log(err); });

(async (url)=> {
	var fs = require('fs');
	var PouchDB = require('pouchdb');
	var db = new PouchDB(url, {skip_setup: true});

	var alldocs = await db.allDocs();
	var dbinfo = await db.info();
	
	try { fs.mkdirSync(dbinfo.db_name); } catch(err) { };
	
	var excludes = ['\\', '/', ':', '*', '?', '<', '>', '|'];

	for (row of alldocs.rows) {
		var doc = await db.get(row.id, {attachments: true});
		delete doc._rev;
		
		var fname = dbinfo.db_name + '/' + encodeURIComponent(row.id) + '.json';
		
		console.log(row.id);
		fs.writeFileSync(fname, JSON.stringify(doc, null, 4));	
	}
	
	console.log('done');
})(url);
