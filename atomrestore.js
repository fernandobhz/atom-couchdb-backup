#! /usr/bin/env node

if (process.argv.length != 3) {
	console.log('Usage: atombackup url');
	return;
}

var url = process.argv[2];

process.on('unhandledRejection', function(err, p){ console.log('unhandledRejection'); console.log(err); });
process.on('uncaughtException', function(err) { console.log('uncaughtException'); console.log(err); });

(async (url)=> {
	var fs = require('fs');
	var PouchDB = require('pouchdb');
	var db = new PouchDB(url);

	var alldocs = await db.allDocs();
	var dbinfo = await db.info();
	
	var files = fs.readdirSync(dbinfo.db_name);
	
	for (file of files) {		
		if ( ! file.endsWith('.json') ) {
			continue;
		}
		
		var json = fs.readFileSync(dbinfo.db_name + '/' + file);
		var doc = JSON.parse(json);
		
		console.log(file);
		await db.put(doc);
	}
	
	console.log('done');
})(url);
